import {useState, useEffect} from 'react';
import React from 'react';
import { Button , Card, Form} from 'react-bootstrap';

import {Fragment} from 'react'


import PropTypes from 'prop-types'

import {Link} from 'react-router-dom';
import { useParams } from 'react-router-dom';

import CreateProductView from '../components/CreateProductView';
import UpdateProductView from '../components/UpdateProductView';


export default function ProductCatalog({product}) {
	
	const { _id} = product;

	

 

 	

 	






 

	return (

		 
		      <Card key={_id} id="bg-cardProduct" className="min-vh-50 container-fluid">
		      		<div className="row">
		      	   		 <Card.Body className="col-6 ">
		      	   		     <Card.Title>{product.title}</Card.Title>
		      	   		     <Card.Subtitle>Description:</Card.Subtitle>
		      	   		     <Card.Text>{product.description}</Card.Text>
		      	   		     <Card.Subtitle>Price:</Card.Subtitle>
		      	   		     <Card.Text>PHP {product.price}</Card.Text>
		      	   		     <Button className="bg-primary" as={Link} to={`/products/${_id}`} >Details</Button>
		      	   		    {/* <Card.Text>Enrollees: {count}</Card.Text>
		      	   		     <Card.Text>Seats: {seats}</Card.Text>
		      	   		     <Button id={'btn-enroll-' + id} className="bg-primary" onClick={enroll}>Enroll</Button>*/}
		      	   		 </Card.Body>
		      	   	</div>	 
		      	</Card>

		  
		
		);

}



// "propTypes" - are a good way of checking data type of information between components.
ProductCatalog.propTypes = {
	// "shape" method is used to check if prop object conforms  to a specific "shape"
	product: PropTypes.shape({
		// Defined properties and their expected types
		title: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
	

		price: PropTypes.number.isRequired
	})
}