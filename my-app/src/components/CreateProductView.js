import { useState,  useContext } from "react";
import { Navigate, useNavigate } from "react-router-dom";
import UserContext from "../UserContext";
import { Form, Button } from "react-bootstrap";
import Swal from "sweetalert2";

import { Link } from 'react-router-dom'

export default function CreateProductView() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();

  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [isActive] = useState(true);
  const [productId] = useState("");


  

  const createProduct = (e) => {
    e.preventDefault();

    if (user.isAdmin){
      fetch(`${process.env.REACT_APP_API_URL}/products/createProduct`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`
        },
        body: JSON.stringify({
          title: title,
          description: description,
          price: price,
          isActive: isActive,
          productId: productId
        })
      })
        .then(res => res.json())
        .then(data => {
          console.log(data);

          if(data !== null){
           // Clear input fields
            setTitle("");
            setDescription("");
            setPrice("");

            Swal.fire({
              title: "Product created successfully",
              icon: "success",
              text: "Your product has been created and is now available for purchase."
            });

             navigate("/products");
             
          } else {
            Swal.fire({
             title: "Unauthorized Action",
              icon: "error",
             text: "Only administrators can create products."
          });
        }
      })
    }
   }     

    

return(
        (user.isAdmin === false || user.id === null) ?
         <Navigate to ="/login" /> 
         :
        <div className="container-fluid min-vh-50 text-center col-md-6 mx-auto shadow-lg m-3">
              <h1>Create a new product</h1>
                   
                 <Form className="" onSubmit={createProduct}>
                   <Form.Group>
                         <Form.Label>Title</Form.Label>
                         <Form.Control
                         type="text"
                          placeholder="Enter product title"
                          value={title}
                          onChange={e => setTitle(e.target.value)}
                            />
                      </Form.Group>
                      <Form.Group>
                      <Form.Label>Description</Form.Label>
                        <Form.Control
                          type="text"
                          placeholder="Enter product description"
                          value={description}
                          onChange={e => setDescription(e.target.value)}
                          />
                      </Form.Group>
                      <Form.Group>
                        <Form.Label>Price</Form.Label>
                        <Form.Control
                          type="number"
                          placeholder="Enter product price"
                          value={price}
                          onChange={e => setPrice(e.target.value)}
                          />
                      </Form.Group>
                     {/* <Form.Group>
                        <Form.Check
                          type="checkbox"
                          label="Is Active"
                          checked={isActive}
                          onChange={e => setIsActive(e.target.checked)}
                           />
                    </Form.Group>*/}
                      <Button variant="primary" type="submit">
                          Create Product
                     </Button>
                      <Link to='/create-product'></Link>
               </Form>
          </div>
    )
  }