import { useState,  useContext } from "react";
import { Navigate } from "react-router-dom";
import { useParams } from 'react-router-dom';

import UserContext from "../UserContext";

import { Form } from "react-bootstrap";

import Swal from "sweetalert2";







export default function UpdateProductView() {

	
  		
  const { productId } = useParams();
 
  const [price, setPrice] = useState("");


 
  const { user } = useContext(UserContext);
 
 
  



  /*function updateProduct (e) {
  	 e.preventDefault();
  			console.log(productId)
          	    //Fetch the product data from the API
          	    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
          	      .then(res => res.json())
          	      .then(data => {
          	        // set the product data in the state
          	        setProductData(data);
          	        setSelectedProductId(productId);
          	        // navigate to the update product view
   
          	      });
          	  

  	  const handleSubmit = () => {
  	    e.preventDefault();
  	    // Get the updated product data from the form inputs
  	    const updatedProductData = {
  	     
  	      price: productData.price.value
  	    }
 
  	     fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
			method: 'PATCH',
			headers: {
			'Content-Type': 'application/json'
			},
				body: JSON.stringify({
				// title: productData.title,
				// description: productData.description,
				price: productData.price
				})
				})
			.then(res => res.json())
			.then(data => {
			console.log(data, 'Product updated');
			setProductData(data);
			 setSelectedProductId(productId);
			// navigate to the admin dashboard view
			})
			.catch(err => {
			console.log(err);
			});
			}
		}*/


  	function updateProduct(e) {
  	        
  	        e.preventDefault()
  	        
  	        console.log(productId)
  	        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
  	           method: "PATCH",
  	           headers: {
  	               'Content-Type' : 'application/json'
  	           },
  	           body: JSON.stringify({
  	            
  	            price: price
  	           })
  	        })
  	        .then(res=> res.json())
  	        .then(data => {
  	           console.log(data)

  	           if(data === true) {

  	               setPrice("");

  	               Swal.fire({
  	                       title: "Product Updated!",
  	                       icon: "success",
  	                       text: "Nice!"
  	                   }).then(()=>{window.location.reload();})

  	                
  	           } else {

  	               Swal.fire({
  	                   title: "Something went wrong",
  	                   icon: "error",
  	                   text: "Please, try again."
  	               }).then(()=>{window.location.reload();})
  	           }
  	        })
  	        
  	    }




  	/*  const handlePriceChange = (e) => {
  	    setProductData({...productData, price: e.target.value});
  	  }
        */
    

 


	return(

		(user.isAdmin === false || user.id === null) ?
		 <Navigate to ="/login" /> 
		 :
	
		<div>
		      <Form onSubmit={(e) => updateProduct(e)}>
		
		        <Form.Label>
		          Price:
		          <input type="number" value={price} onChange={(e) => {setPrice(e.target.value)}}  />
		        </Form.Label>
		        <br />
		        <button type='submit '>Update Product</button>
				</Form>
		</div>
			
	)
}







/*import { useParams, useHistory } from 'react-router-dom';
import { useEffect, useState } from "react";


export default function UpdateProductView() {
  const [product, setProduct] = useState({});
  const [title, setTitle] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const { productId } = useParams();
	 const [newData, setNewData] = useState({});


useEffect(() => {
    // Make a GET request to your API to retrieve the product information
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
        .then(res => res.json())
        .then(data => {
            // Set the product information to the state
            setNewData({
                title: data.title,
                description: data.description,
                price: data.price
            });
        })
        .catch(err => console.error(err));
}, [productId]);

  const handleTitleChange = (e) => {
    setNewData({...newData, title: e.target.value});
  }

  const handleDescriptionChange = (e) => {
    setNewData({...newData, description: e.target.value});
  }

  const handlePriceChange = (e) => {
    setNewData({...newData, price: e.target.value});
}

const handleChange = (event) => {
setNewData({...newData, [event.target.name]: event.target.value})
}


return (
    <form>
      <label>
        Title:
        <input type="text" value={title} onChange={(e) => setTitle(e.target.value)}  />
      </label>
      <label>
        Description:
        <textarea value={description} onChange={(e) => setDescription(e.target.value)} />
      </label>
      <label>
        Price:
        <input type="number" value={price} onChange={(e) => setPrice(e.target.value)} />
      </label>
      <button type="submit">Update Product</button>
    </form>
  );
}*/